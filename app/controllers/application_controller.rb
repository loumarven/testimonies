class ApplicationController < ActionController::Base
  include SessionsHelper

  def hello
    render html: "Hello, world!"
  end

  def about
    render html: "About"
  end

  def tags
    render html: "Tags"
  end
end
